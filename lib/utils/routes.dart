import 'package:chat_app/ui/screens/login_screen.dart';
import 'package:chat_app/ui/screens/signup_screen.dart';
import 'package:chat_app/ui/screens/welcome_screen.dart';
import 'package:flutter/material.dart';

class Routes {
  static final Map<String, WidgetBuilder> appRoutes = <String, WidgetBuilder>{
    LoginScreen.routeName: (BuildContext context) => LoginScreen(),
    SignupScreen.routeName: (BuildContext context) => SignupScreen(),
    WelcomeScreen.routeName: (BuildContext context) => WelcomeScreen(),
  };
}
