import 'package:chat_app/ui/screens/login_screen.dart';
import 'package:chat_app/ui/screens/signup_screen.dart';
import 'package:chat_app/ui/screens/widgets/primary_button.dart';
import 'package:chat_app/ui/size_config.dart';
import 'package:chat_app/ui/style.dart';
import 'package:flutter/material.dart';

class WelcomeScreen extends StatelessWidget {
  static final String routeName = 'welcome';
  const WelcomeScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      backgroundColor: Style.darkColor,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
            child: Center(
              child: Text(
                'Flutter',
                style: Style.appNameTextStyle,
              ),
            ),
          ),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                PrimaryButton(
                  onTap: () =>
                      Navigator.of(context).pushNamed(LoginScreen.routeName),
                  child: Text(
                    'Sign in',
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 21),
                  ),
                ),
                SizedBox(
                  height: SizeConfig.blockSizeHorizontal * 2,
                ),
                PrimaryButton(
                  onTap: () =>
                      Navigator.of(context).pushNamed(SignupScreen.routeName),
                  child: Text(
                    'Sign up',
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 21),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
