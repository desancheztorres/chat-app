import 'package:chat_app/ui/screens/widgets/primary_button.dart';
import 'package:chat_app/ui/size_config.dart';
import 'package:chat_app/ui/style.dart';
import 'package:flutter/material.dart';

class LoginScreen extends StatefulWidget {
  static final String routeName = "login";
  const LoginScreen({Key key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _key = GlobalKey<FormState>();

  static RegExp emailRegExp = RegExp(
      r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+");

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      backgroundColor: Style.darkColor,
      body: ListView(
        children: <Widget>[
          SizedBox(
            height: SizeConfig.screenHeight * 0.1,
          ),
          Center(
            child: Text(
              'Sign in',
              style: Style.appNameTextStyle,
            ),
          ),
          SizedBox(
            height: SizeConfig.screenHeight * 0.1,
          ),
          Padding(
            padding: EdgeInsets.symmetric(
              horizontal: SizeConfig.blockSizeHorizontal * 4,
            ),
            child: Form(
              key: _key,
              child: Column(
                children: <Widget>[
                  TextFormField(
                    validator: (v) {
                      if (v.isEmpty) {
                        return 'input required';
                      }

                      if (!emailRegExp.hasMatch(v)) {
                        return 'email is not valid';
                      }

                      return null;
                    },
                    decoration: Style.inputDecoration(label: "Email"),
                    onSaved: (v) {},
                  ),
                  SizedBox(
                    height: SizeConfig.blockSizeVertical * 2,
                  ),
                  TextFormField(
                    obscureText: true,
                    validator: (v) {
                      if (v.isEmpty) {
                        return 'input required';
                      }

                      if (v.trim().length < 6) {
                        return 'this password is too short';
                      }

                      return null;
                    },
                    decoration: Style.inputDecoration(label: "Password"),
                    onSaved: (v) {},
                  ),
                  SizedBox(
                    height: SizeConfig.blockSizeVertical * 2,
                  ),
                ],
              ),
            ),
          )
        ],
      ),
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.symmetric(vertical: 12.0),
        child: PrimaryButton(
          onTap: () {
            if (_key.currentState.validate()) {
              _key.currentState.save();
              print("is valid!");
            }
          },
          child: Text(
            "Login",
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 21.0,
            ),
          ),
        ),
      ),
    );
  }
}
