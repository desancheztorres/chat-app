import 'package:chat_app/ui/screens/conversations_screen.dart';
import 'package:chat_app/ui/style.dart';
import 'package:flutter/material.dart';
import 'package:bubble_bottom_bar/bubble_bottom_bar.dart';

class MainScreen extends StatefulWidget {
  static final String routeName = "main";
  MainScreen({Key key}) : super(key: key);

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  final PageController _pageController = PageController();
  int currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        physics: NeverScrollableScrollPhysics(),
        controller: _pageController,
        children: <Widget>[
          ConversationsScreen(),
          Container(
            color: Colors.green,
          ),
          Container(
            color: Colors.blue,
          )
        ],
      ),
      bottomNavigationBar: BubbleBottomBar(
        backgroundColor: Style.darkColor,
        opacity: .2,
        currentIndex: currentIndex,
        onTap: (i) {
          setState(() {
            currentIndex = i;
            _pageController.animateToPage(
              i,
              duration: kTabScrollDuration,
              curve: Curves.easeIn,
            );
          });
        },
        elevation: 8,
        inkColor: Colors.black12,
        items: <BubbleBottomBarItem>[
          BubbleBottomBarItem(
            backgroundColor: Colors.red,
            icon: Icon(
              Icons.chat_bubble_outline,
              color: Colors.white,
            ),
            activeIcon: Icon(
              Icons.chat_bubble_outline,
              color: Colors.red,
            ),
            title: Text(
              "Home",
              style: TextStyle(
                color: Colors.white,
              ),
            ),
          ),
          BubbleBottomBarItem(
            backgroundColor: Colors.deepPurple,
            icon: Icon(
              Icons.people_outline,
              color: Colors.white,
            ),
            activeIcon: Icon(
              Icons.people_outline,
              color: Colors.deepPurple,
            ),
            title: Text("Friends"),
          ),
          BubbleBottomBarItem(
            backgroundColor: Colors.indigo,
            icon: Icon(
              Icons.person_outline,
              color: Colors.white,
            ),
            activeIcon: Icon(
              Icons.person_outline,
              color: Colors.indigo,
            ),
            title: Text("Profile"),
          ),
        ],
      ),
    );
  }
}
