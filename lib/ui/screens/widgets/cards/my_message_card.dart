import 'package:chat_app/ui/style.dart';
import 'package:flutter/material.dart';

class MyMessageCard extends StatelessWidget {
  const MyMessageCard({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 310.0,
      padding: EdgeInsets.all(21.0),
      margin: EdgeInsets.only(bottom: 12),
      decoration: BoxDecoration(
        color: Style.darkColor,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(28.0),
          topRight: Radius.circular(28.0),
          bottomLeft: Radius.circular(28.0),
        ),
      ),
      child: Row(
        children: <Widget>[
          Expanded(
            child: Text(
                'jsadhkwqbsakdbwksasfds jsdjhbshjbds ksjdbjskbdhsbdhk sjkdbksjbdhsbd skjdbhjskbdhs fdssfsaf sakjdbjshab dsajkdbhsabdh sadbahsjbdjhsab dksajbdkasfdsvds dsvfdsfhbdkhabk'),
          ),
        ],
      ),
    );
  }
}
