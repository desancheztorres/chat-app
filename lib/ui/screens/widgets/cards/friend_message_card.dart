import 'package:chat_app/ui/style.dart';
import 'package:flutter/material.dart';

class FriendMessageCard extends StatelessWidget {
  const FriendMessageCard({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(bottom: 12.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          CircleAvatar(
            backgroundImage: NetworkImage(
                'https://yt3.ggpht.com/a-/AOh14GghxtuD3RLriHVOz2xAUzibjG73sOngkXy7zARAtKo=s88-c-k-c0xffffffff-no-rj-mo'),
          ),
          SizedBox(width: 12.0),
          Container(
            width: 290.0,
            padding: EdgeInsets.all(21.0),
            decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [
                  Style.primaryColor,
                  Style.secondaryColor,
                  Style.primaryColor,
                ],
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
              ),
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(28.0),
                topRight: Radius.circular(28.0),
                bottomRight: Radius.circular(28.0),
              ),
            ),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Text(
                      'sakjdbjshab sdhjsgdjhs shdhjsbdj jshabdjhsabj sjhadghjsa dsabdjhsabjhdbasn dnmsabdhsahbdjhsa dsajkdbhsabdh sadbahsjbdjhsab dksajbdkasfdsvds dsvfdsfhbdkhabk'),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
