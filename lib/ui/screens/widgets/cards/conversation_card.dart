import 'package:flutter/material.dart';

class ConversationCard extends StatelessWidget {
  final Function onTap;
  const ConversationCard({
    Key key,
    @required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: onTap,
      leading: ClipOval(
        child: Image.network(
          'https://yt3.ggpht.com/a-/AOh14GghxtuD3RLriHVOz2xAUzibjG73sOngkXy7zARAtKo=s88-c-k-c0xffffffff-no-rj-mo',
        ),
      ),
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            "Cristian",
            style: TextStyle(
              color: Colors.white.withOpacity(0.9),
              fontSize: 19.0,
            ),
          ),
          Text(
            "12:12 am",
            style: TextStyle(
              color: Colors.white.withOpacity(0.7),
              fontSize: 14.0,
            ),
          ),
        ],
      ),
      subtitle: Text("Hey Bro"),
    );
  }
}
