import 'package:chat_app/ui/screens/chat_screen.dart';
import 'package:chat_app/ui/screens/widgets/cards/conversation_card.dart';
import 'package:chat_app/ui/size_config.dart';
import 'package:chat_app/ui/style.dart';
import 'package:flutter/material.dart';

class ConversationsScreen extends StatefulWidget {
  ConversationsScreen({Key key}) : super(key: key);

  @override
  _ConversationsScreenState createState() => _ConversationsScreenState();
}

class _ConversationsScreenState extends State<ConversationsScreen> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Style.darkColor,
        title: Text("Conversations"),
        centerTitle: true,
        leading: IconButton(
          icon: Icon(Icons.search),
          onPressed: () {},
        ),
      ),
      backgroundColor: Colors.black,
      body: Center(
        child: ListView(
          padding: EdgeInsets.only(top: SizeConfig.blockSizeVertical * 2),
          children: <Widget>[
            ConversationCard(
              onTap: () => Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (_) => ChatScreen(),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
