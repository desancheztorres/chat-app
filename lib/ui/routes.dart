import 'package:chat_app/ui/screens/login_screen.dart';
import 'package:chat_app/ui/screens/main_screen.dart';
import 'package:chat_app/ui/screens/signup_screen.dart';
import 'package:chat_app/ui/screens/welcome_screen.dart';
import 'package:flutter/material.dart';

const String initialRoute = "main";

class Router {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case 'welcome':
        return MaterialPageRoute(builder: (_) => WelcomeScreen());
        break;

      case 'login':
        return MaterialPageRoute(builder: (_) => LoginScreen());
        break;

      case 'sign-up':
        return MaterialPageRoute(builder: (_) => SignupScreen());
        break;

      case 'main':
        return MaterialPageRoute(builder: (_) => MainScreen());
        break;

      default:
        return MaterialPageRoute(
          builder: (_) => Scaffold(
            body: Center(
              child: Text('No route defined for ${settings.name}'),
            ),
          ),
        );
    }
  }
}
